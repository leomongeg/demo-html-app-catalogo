//
//  ViewController.m
//  Bellaface
//
//  Created by Jorge Leonardo Monge García on 1/14/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import "ViewController.h"
#import <GPUImage.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ViewController ()

@property (nonatomic, strong)WebViewJavascriptBridge *bridge;

@property (nonatomic, strong) GPUImageView *myView;
@property (nonatomic, strong) GPUImageMovie *movieFile;
@property (nonatomic, strong) GPUImagePicture *sourcePicture;
@property (nonatomic, strong) GPUImageOutput<GPUImageInput> *filter;
@property (nonatomic, strong) WVJBResponseCallback endVideoPlayCallback;

@property(nonatomic, strong) AVAudioPlayer *audioPlayer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _endVideoPlayCallback = nil;
    [self initBridge];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"www"]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    UIView *firstView = [_webView.subviews firstObject];
    
    if ([firstView isKindOfClass:[UIScrollView class]]) {
        
        UIScrollView *scroll = (UIScrollView*)firstView;
        [scroll setBounces:NO]; //to stop bouncing
        
    }
    
}

- (BOOL)prefersStatusBarHidden { return YES; }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initBridge
{
//    [WebViewJavascriptBridge enableLogging];
    
    _bridge = [WebViewJavascriptBridge bridgeForWebView:_webView webViewDelegate:self handler:^(id data, WVJBResponseCallback responseCallback){
        NSLog(@"Datos recibidos %@", data);
        responseCallback(@"Response for message from ObjC");
    }];
    
    [_bridge registerHandler:@"showVideo" handler:^(id data, WVJBResponseCallback responseCallback) {
        [self playTransparentVideo:data andResponseCallback:responseCallback];
    }];
    
    [_bridge registerHandler:@"playFullScreenVideo" handler:^(id data, WVJBResponseCallback responseCallback) {
        [self playFullScreenVideo:[data objectForKey:@"resource"]];
    }];
}

- (void)playVideo:(NSString *)resource withBackground:(UIImage *)background andFrame:(CGRect)frame
{
    if(_myView == nil)
    {
        _myView = [[GPUImageView alloc] initWithFrame:CGRectZero];
    }
    
    [self.view addSubview:_myView];
    [_myView setFrame:frame];
    
    NSURL *resURL = [[NSBundle mainBundle] URLForResource:resource withExtension:@"mov" subdirectory:@"Media"];
    
    _movieFile = [[GPUImageMovie alloc] initWithURL:resURL];
    
    _filter = [[GPUImageChromaKeyBlendFilter alloc] init];
    [(GPUImageChromaKeyBlendFilter *)_filter setColorToReplaceRed:(132.0f/255) green:(96.0f/255) blue:(159.0f/255)];
    [(GPUImageChromaKeyBlendFilter *)_filter setThresholdSensitivity:0.01];
    
    [_movieFile addTarget:_filter];
    UIImage *inputImage = background;
    [_myView setHidden:NO];
    _sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
    [_sourcePicture addTarget:_filter];
    [_sourcePicture processImage];
    
    GPUImageView *filterView = _myView;
    [_filter addTarget:filterView];
    _movieFile.playAtActualSpeed = YES;
    filterView.fillMode = kGPUImageFillModeStretch;
    [_movieFile startProcessing];
    
    NSString *path = [[NSBundle mainBundle]
                      pathForResource:resource ofType:@"mp3" inDirectory:@"Media"];
    _audioPlayer   = [[AVAudioPlayer alloc]initWithContentsOfURL:
                      [NSURL fileURLWithPath:path] error:NULL];
    [_audioPlayer setDelegate:self];
    [_audioPlayer play];
}

-(UIImage*) makeImageWithSize:(CGSize)size andOrigin:(CGPoint)point {
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    if(screenSize.height > screenSize.width)
    {
        screenSize = CGSizeMake(screenSize.height, screenSize.width);
    }
    
    UIGraphicsBeginImageContext(screenSize);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *sourceImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContext(size);
    [sourceImage drawAtPoint:CGPointMake(-point.x, -point.y)];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //    UIImageWriteToSavedPhotosAlbum(croppedImage,nil, nil, nil);
    
    return croppedImage;
}


-(void)playTransparentVideo:(id)data andResponseCallback:(WVJBResponseCallback) responseCallback
{
    NSDictionary *params = data;
    CGSize size          = CGSizeMake([[params objectForKey:@"w"] floatValue], [[params objectForKey:@"h"] floatValue]);
    CGPoint origin       = CGPointMake([[params objectForKey:@"x"] floatValue], [[params objectForKey:@"y"] floatValue]);
    UIImage *image       = [self makeImageWithSize:size andOrigin:origin];
    
    CGRect frame         = CGRectMake(origin.x, origin.y, size.width, size.height);
//    UIImageView *imgView = [[UIImageView alloc]initWithImage:image]; //Para pruebas de pos del background
//    [imgView setFrame:frame]; //Para pruebas de pos del background
    
//    [self playVideo:[params objectForKey:@"resource"] withBackground:image andFrame:frame];
//    Por razones de demo y alivianar el tamaño del repositorio, se va a reproducir la secuencia 1 unicamente. Por parametro "resource" se define el nombre del recurso a reproducir.
    [self playVideo:@"sequence-purple1" withBackground:image andFrame:frame];
    _endVideoPlayCallback = responseCallback;
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player
                       successfully:(BOOL)flag
{
    [_myView removeFromSuperview];
    if(_endVideoPlayCallback != nil)
    {
        _endVideoPlayCallback(@"Video Finished");
    }
}

- (void)playFullScreenVideo:(NSString *)resource
{
    resource = @"sequence-purple1"; //Para efectos de demostración y alivianar el tamaño del repositiorio y del proyecto, se va a reproducir el video sequence-purple1.mov
    NSString *path = [[NSBundle mainBundle]
                      pathForResource:resource ofType:@"mov" inDirectory:@"Media"];
    
    MPMoviePlayerViewController *movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:path]];
    [self presentMoviePlayerViewControllerAnimated:movieController];
    [movieController.moviePlayer play];
}

@end
