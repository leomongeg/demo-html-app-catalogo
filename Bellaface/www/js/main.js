window.onload = function () {
    _sceneManager = new Scene();
    _sceneManager.initNav();
    _sceneManager.loadScene("landing");
};

var Scene = function () {
    var self                = this;
    this.sceneIdx           = 0;
    this.navigation         = $("#navigation");
    this.sceneContainer     = $("#scenes");
    this.referenceContainer = $("#reference_box");
    this.navlock            = false;
    this._landing;
    this._dosmundos;
    this._antiandrogenica;
    this._menosefectos;
    this._eficaciaclinica;
    this._modeloanticonceptivo;
    this._algoqueaprender;
    this._bellezasinriesgo;
    
    this.Landing = function () {
        var _scope = this;
        
        this.animStack = [];
        
        this.buttons = $("#landing .button");
        this.square  = $("#landing .square");
        this.title   = $("#landing .title");
        this.phraseA = this.title.find(".first");
        this.phraseB = this.title.find(".second");

        this.start = function () {
            var anim0 = TweenMax.to(_scope.square, 1, {y:0, rotation:45, opacity:1, force3D:true});
            var anim1 = TweenMax.to(_scope.title, .5, {opacity:1, delay:1, force3D:true});
            var anim2 = TweenMax.staggerTo(_scope.buttons, .7, {scale:1, ease:Back.easeOut, force3D:true}, .2);
            _scope.animStack.push(anim0, anim1, anim2);
        };
        
        this.reset = function () {
            self._reset(_scope.animStack);
            _scope.animStack.length = 0;
            _scope.phraseA.removeClass("toggle");
            _scope.phraseB.removeClass("toggle");
        };
        
        this.square.on("click", changePhrase);
        
        function changePhrase() {
            var anim0 = TweenMax.to(_scope.phraseA, .5, {y:-200, opacity:0, force3D:true, onComplete: function () {
                _scope.phraseA.addClass("toggle");
                _scope.phraseB.addClass("toggle");
                var anim0 = TweenMax.to(_scope.phraseB, .5, {y:0, opacity:1, force3D:true});
                _scope.animStack.push(anim0);
            }});
            _scope.animStack.push(anim0);
        }
    };
    
    this.Dosmundos = function () {
        var _scope = this;
        
        this.animStack = [];
        
        this.infoLeft  = $("#dosmundos .col-left .trans-to");
        this.infoRight = $("#dosmundos .col-right .trans-to");
        this.txt       = $("#dosmundos .txt0 .text");
        this.txtBlocks = $("#dosmundos .txt0 .block");

        this.start = function () {
            var anim0 = TweenMax.staggerTo(_scope.infoLeft, .5, {x:0, opacity:1, force3D:true}, .2, _scope.second);
            var anim1 = TweenMax.staggerTo(_scope.infoRight, .5, {x:0, opacity:1, force3D:true}, .2);
            _scope.animStack.push(anim0, anim1);
        };
        
        this.second = function () {
            var anim0 = TweenMax.to(_scope.txtBlocks, .5, {width:120, height:72, force3D:true, onComplete: function () {
                var anim0 = TweenMax.to(_scope.txt, .5, {opacity:1, force3D:true});
                _scope.animStack.push(anim0);
            }});
            _scope.animStack.push(anim0);
        };
        
        this.reset = function () {
            self._reset(_scope.animStack);
            _scope.animStack.length = 0;
        };
    };
    
    this.Antiandrogenica = function () {
        var _scope   = this;
        var _heights = [158, 233, 37, 55, 16, 12];
        
        this.animStack = [];
        
        this.photos      = $("#antiandrogenica .photo");
        this.txt         = $("#antiandrogenica .txt0");
        this.bars        = $("#antiandrogenica .bar-fill");
        this.percent     = $("#antiandrogenica .percent");
        this.indications = $("#antiandrogenica .ind");
        this.likehand    = $("#antiandrogenica .like");
        
        this.start = function () {
            var anim0 = TweenMax.staggerTo(_scope.photos, .5, {x:0, scale:1, opacity:1, force3D:true}, .2, function () {
                $("#antiandrogenica .photos").addClass("active");
            });
            var anim1 = TweenMax.to(_scope.txt, .5, {x:0, opacity:1, force3D:true, onComplete: function () {
                for (var i = 0, l = _scope.bars.length; i < l; i++) {
                    var anim0 = TweenMax.to(_scope.bars[i], .7, {height:_heights[i], force3D:true, onComplete: function () {
                        _scope.percent.addClass("show");
                        _scope.indications.addClass("show");
                    }}); 
                    _scope.animStack.push(anim0);
                };
                for (var i = 0, l = _scope.likehand.length; i < l; i++) {
                    var element = _scope.likehand[i];
                    var anim0 = TweenMax.to(element, .5, {opacity:1, y:0, force3D:true, ease: Power0.easeNone, onComplete: function (el){
                        var anim1 = TweenMax.to(el, .5, {opacity:0, y:-75, force3D:true, ease: Power0.easeNone}); 
                    }, onCompleteParams: [element]}); 
                    _scope.animStack.push(anim0);
                };
            }});
            _scope.animStack.push(anim0, anim1);
        };
        
        this.reset = function () {
            self._reset(_scope.animStack);
            _scope.percent.removeClass("show");
            _scope.indications.removeClass("show");
            $("#menosefectos .photos").removeClass("active");
            _scope.animStack.length = 0;
            _scope.likehand.removeAttr("style");
        };
    };
    
    this.Menosefectos = function () {
        var _scope = this;
        
        this.animStack = [];
        this.to;
        
        this.photos      = $("#menosefectos .photo");
        this.donuts      = $("#menosefectos .donut");
        this.description = $("#menosefectos .description");
        this.woman       = $("#womanplay_a");
        this.video       = $("#video_a");
        
        this.start = function () {
            var anim0 = TweenMax.staggerTo(_scope.photos, .5, {x:0, scale:1, opacity:1, force3D:true}, .2, function () {
                $("#menosefectos .photos").addClass("active");
            });
            var anim1 = TweenMax.staggerTo(_scope.donuts, .5, {x:0, opacity:1, force3D:true, onComplete: _scope.paintLine, onCompleteParams:["{self}"]}, .2, function () {
                _scope.description.addClass("active");
                _scope.woman.addClass("active");
            });
            _scope.animStack.push(anim0, anim1);
        };
        
        this.paintLine = function (tween) {
            $(tween.target).find(".donut-line").lazylinepainter("paint");
        };
        
        this.reset = function () {
            self._reset(_scope.animStack);
            eraseLines();
            _scope.woman.removeClass("active");
            _scope.description.removeClass("active");
            $("#menosefectos .photos").removeClass("active");
            clearTimeout(_scope.to);
            _scope.animStack.length = 0;
        };
        
        function eraseLines() {
            _donutA.lazylinepainter("erase");
            _donutB.lazylinepainter("erase");
            _donutC.lazylinepainter("erase");
            _donutD.lazylinepainter("erase");
            _donutE.lazylinepainter("erase");
            _donutF.lazylinepainter("erase");
        };
        
        this.playVideo = function () {
            self.navlock = true;
            _scope.woman.removeClass("active");
            _scope.to = setTimeout(function () {
                _bridge.playAlphaVideo(_scope.video, {callback:function () {
                    self.navlock = false;
                    _scope.woman.addClass("active");
                }});
            }, 700);
        };
        
        _scope.woman.on("click", _scope.playVideo);
    };
    
    this.Eficaciaclinica = function () {
        var _scope = this;
        
        this.animStack = [];
        this.to;
        
        this.photos    = $("#eficaciaclinica .photo");
        this.buttons   = $("#eficaciaclinica .button");
        this.woman     = $("#womanplay_b");
        this.video     = $("#video_b");
        this.videoFull = $("#eficaciaclinica .video-full");
        
        this.start = function () {
            var anim0 = TweenMax.to(_scope.photos, .5, {x:0, scale:1, opacity:1, force3D:true, onComplete: function () {
                $("#eficaciaclinica .photos").addClass("active");
            }});
            var anim1 = TweenMax.to(_scope.buttons, .5, {x:0, rotation:0, scale:1, delay: .7, ease: Back.easeOut, force3D:true, onComplete: function () {
                _scope.woman.addClass("active");
            }});
            _scope.animStack.push(anim0, anim1);
        };
        
        this.reset = function () {
            self._reset(_scope.animStack);
            _scope.woman.removeClass("active");
            $("#eficaciaclinica .photos").removeClass("active");
            _scope.animStack.length = 0;
        };
        
        this.playVideo = function () {
            self.navlock = true;
            _scope.woman.removeClass("active");
            _scope.to = setTimeout(function () {
                _bridge.playAlphaVideo(_scope.video, {callback:function () {
                    self.navlock = false;
                    _scope.woman.addClass("active");
                }});
            }, 700);
        };
        
        this.playFullscreenVideo = function () {
            var resource = $(this).data("video");
            _bridge.playFullScreenVideo(resource);
        };
        
        _scope.woman.on("click", _scope.playVideo);
        _scope.videoFull.on("click", _scope.playFullscreenVideo);
    };
    
    this.Modeloanticonceptivo = function () {
        var _scope = this;
        
        this.animStack = [];
        this.to;
        
        this.photos          = $("#modeloanticonceptivo .photo");
        this.info            = $("#modeloanticonceptivo .trans-to");
        this.woman           = $("#womanplay_c");
        this.video           = $("#video_c");
        this.referenceButton = $("#reference_button");
        this.referenceBox    = $("#reference_box");
        
        this.start = function () {
            var anim0 = TweenMax.staggerTo(_scope.photos, .5, {y:0, opacity:1, ease:Back.easeOut, force3D:true}, .2, function () {
                $("#modeloanticonceptivo .photos").addClass("active");
            });
            var anim1 = TweenMax.staggerTo(_scope.info, .5, {x:0, y:0, rotation:0, scale:1, delay:.7, ease:Back.easeOut, force3D:true}, .2, function () {
                _scope.woman.addClass("active");
            });
            _scope.animStack.push(anim0, anim1);
        };
        
        this.reset = function () {
            self._reset(_scope.animStack);
            _scope.woman.removeClass("active");
            $("#modeloanticonceptivo .photos").removeClass("active");
            _scope.animStack.length = 0;
        };
        
        this.playVideo = function () {
            self.navlock = true;
            _scope.woman.removeClass("active");
            _scope.to = setTimeout(function () {
                _bridge.playAlphaVideo(_scope.video, {callback:function () {
                    self.navlock = false;
                    _scope.woman.addClass("active");
                }});
            }, 700);
        };
        
        this.toggleReferenceBox = function () {
            self.referenceContainer.toggleClass("active");
        };
        
        _scope.woman.on("click", _scope.playVideo);
        _scope.referenceButton.on("click", _scope.toggleReferenceBox);
    };
    
    this.Algoqueaprender = function () {
        var _scope = this;
        
        this.animStack = [];
        
        this.videos = $("#algoqueaprender .video");
        
        this.start = function () {
            var anim0 = TweenMax.staggerTo(_scope.videos, .5, {x:0, force3D:true, onComplete: showIcon, onCompleteParams:["{self}"]}, .2);
            _scope.animStack.push(anim0);
        };
        
        function showIcon(tween) {
            $(tween.target).find(".play").addClass("active");
        };
        
        this.reset = function () {
            self._reset(_scope.animStack);
            $("#algoqueaprender .video .play").removeClass("active");
            _scope.animStack.length = 0;
        };
        
        function playVideo() {
            var resource = $(this).data("video");
            _bridge.playFullScreenVideo(resource);
            console.log("hola", this);
        }
        
        _scope.videos.on("click", playVideo);
    };
    
    this.Bellezasinriesgo = function () {
        var _scope = this;
        
        this.animStack = [];
        this.to;
        
        this.info = $("#bellezasinriesgo .trans-to");
        this.rows = $("#bellezasinriesgo .row");
        
        this.start = function () {
            _scope.to = setTimeout(function () {
                var anim0 = TweenMax.staggerTo(_scope.rows, .5, {y:0, opacity:1, force3D:true, onComplete: showIcon, onCompleteParams:["{self}"]}, .5);    
                _scope.animStack.push(anim0);
            }, 1300);
            
            var anim0 = TweenMax.staggerTo(_scope.info, .5, {x:0, y:0, scale:1, force3D:true}, .2);
            _scope.animStack.push(anim0);
        };
        
        function showIcon(tween) {
            $(tween.target).find(".check").lazylinepainter("paint");
        };
        
        this.reset = function () {
            self._reset(_scope.animStack);
            _checkA.lazylinepainter("erase");
            _checkB.lazylinepainter("erase");
            _checkC.lazylinepainter("erase");
            clearTimeout(_scope.to);
            _scope.animStack.length = 0;
        };
    };
    
    this._reset = function (animsArray) {
        for (i=0, l=animsArray.length; i<l; i++) {
            var tween = animsArray[i];
            if (Array.isArray(tween)) {
                for (a=0, b=tween.length; a<b; a++) {
                    tween[a].pause(0);
                }
            } else {
                tween.pause(0);
            }
        };
    };
    
    this.prevScene = function () {
        if (self.navlock)
            return;
        
        var current   = self.sceneContainer.find(".scene.active");
        var childrens = self.sceneContainer.children();
        var index     = current.index();
        self.referenceContainer.removeClass("active");
        
        if (index <= 0)
            return false;
        
        var toIdx     = index-1;
        var _y        = -(toIdx) * 1024;
        var toScene   = childrens[toIdx];
        self.sceneIdx = toIdx;
        
        TweenMax.to(self.sceneContainer, .5, {x:_y, force3D:true, onComplete: self.loadScene, onCompleteParams: [toScene.id]});
        
        current.removeClass("active");
        $(toScene).addClass("active");
    };
    
    this.nextScene = function () {
        if (self.navlock)
            return;
        
        var current   = self.sceneContainer.find(".scene.active");
        var childrens = self.sceneContainer.children();
        var index     = current.index();
        self.referenceContainer.removeClass("active");
        
        if (index >= childrens.length - 1)
            return false;
        
        var toIdx     = index+1;
        var _y        = -(toIdx) * 1024;
        var toScene   = childrens[toIdx];
        self.sceneIdx = toIdx;
        
        TweenMax.to(self.sceneContainer, .5, {x:_y, force3D:true, onComplete: self.loadScene, onCompleteParams: [toScene.id]});
        
        current.removeClass("active");
        $(toScene).addClass("active");
    };
    
    this.homeScene = function () {
        if (self.navlock)
            return;
        
        var current   = self.sceneContainer.find(".scene.active");
        var childrens = self.sceneContainer.children();
        var index     = current.index();
        self.referenceContainer.removeClass("active");
        
        if (index == 0)
            return false;
        
        var toScene = childrens[0];
        self.sceneIdx = 0;
        
        TweenMax.to(self.sceneContainer, .5, {x:0, force3D:true, onComplete: self.loadScene, onCompleteParams: [toScene.id]});
        
        current.removeClass("active");
        $(toScene).addClass("active");
    };
    
    this.gotoScene = function(evt, idx) {
        if (self.navlock)
            return;
        
        var current   = self.sceneContainer.find(".scene.active");
        var childrens = self.sceneContainer.children();
        var index     = idx || this.dataset.idx || ((this.dataset.idn)? $("#" + this.dataset.idn).index() : -1);
        self.referenceContainer.removeClass("active");
            
        if (index < 0)
            return false;
        
        var _y      = -(index) * 1024;
        var toScene = childrens[index];
        
        if (current.id === toScene.id)
            return false;
        
        self.sceneIdx = index;
        
        TweenMax.to(self.sceneContainer, .5, {x:_y, force3D:true, onComplete: self.loadScene, onCompleteParams: [toScene.id]});
        
        current.removeClass("active");
        $(toScene).addClass("active");
    }
    
    this.resetScenes = function () {
        if (self.sceneIdx === 1)
            self.navigation.addClass("pink");
        else
            self.navigation.removeClass("pink");
        
        try {
            if (self._landing) self._landing.reset();
            if (self._dosmundos) self._dosmundos.reset();
            if (self._antiandrogenica) self._antiandrogenica.reset();
            if (self._menosefectos) self._menosefectos.reset();
            if (self._eficaciaclinica) self._eficaciaclinica.reset();
            if (self._modeloanticonceptivo) self._modeloanticonceptivo.reset();
            if (self._algoqueaprender) self._algoqueaprender.reset();
            if (self._bellezasinriesgo) self._bellezasinriesgo.reset();
        } catch (err) {
            console.log(err);
        }
    };
    
    this.loadScene = function (scene_name) {
        self.resetScenes();
        
        var scene;
        
        switch (scene_name) {
            case "landing":
                var scene = (self._landing)? self._landing : new self.Landing();
                scene.start();
                self._landing = scene;
                break;
            case "dosmundos":
                var scene = (self._dosmundos)? self._dosmundos : new self.Dosmundos();
                scene.start();
                self._dosmundos = scene;
                break;
            case "antiandrogenica":
                var scene = (self._antiandrogenica)? self._antiandrogenica : new self.Antiandrogenica();
                scene.start();
                self._antiandrogenica = scene;
                break;
            case "menosefectos":
                var scene = (self._menosefectos)? self._menosefectos : new self.Menosefectos();
                scene.start();
                self._menosefectos = scene;
                break;
            case "eficaciaclinica":
                var scene = (self._eficaciaclinica)? self._eficaciaclinica : new self.Eficaciaclinica();
                scene.start();
                self._eficaciaclinica = scene;
                break;
            case "modeloanticonceptivo":
                var scene = (self._modeloanticonceptivo)? self._modeloanticonceptivo : new self.Modeloanticonceptivo();
                scene.start();
                self._modeloanticonceptivo = scene;
                break;
            case "algoqueaprender":
                var scene = (self._algoqueaprender)? self._algoqueaprender : new self.Algoqueaprender();
                scene.start();
                self._algoqueaprender = scene;
                break;
            case "bellezasinriesgo":
                var scene = (self._bellezasinriesgo)? self._bellezasinriesgo : new self.Bellezasinriesgo();
                scene.start();
                self._bellezasinriesgo = scene;
                break;
            default:
                break;
        }
    };
    
    this.initNav = function () {
        $("#nav_prev").on("click", self.prevScene);
        $("#nav_home").on("click", self.homeScene);
        $("#nav_next").on("click", self.nextScene);
        $(".evt_goto").on("click", self.gotoScene);
        
        $("#scenes").swipe({
            swipe: function (event, direction, distance, duration, fingerCount) {
                if (direction === "up" && fingerCount === 3)
                    self.homeScene();
                else if (direction === "left" && fingerCount === 1)
                    self.nextScene();
                else if (direction === "right" && fingerCount === 1)
                    self.prevScene();
            },
            fingers:'all'
        });
    };
};