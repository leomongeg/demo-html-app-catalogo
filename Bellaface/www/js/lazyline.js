var _donutA, _donutB, _donutC, _donutD, _donutE, _donutF, _checkA, _checkB, _checkC;

$(function () {
    var donutA = {
        "donut_a": {
            "strokepath": [
            {
                "path": "M2,2  c7.2,0.5,14.4,2.1,21.4,4.9c35,14.3,51.8,54.2,37.6,89.2c-3.5,8.6-8.5,16.1-14.7,22.3",
                "duration": 600
            }
        ],
        "dimensions": {
            "width": 68,
            "height": 121
        }
        }
    };
    var donutB = {
        "donut_b": {
            "strokepath": [
                {
                    "path": "M100,4c90.529,0,163.918,73.39,163.918,163.919c0,90.528-73.389,163.918-163.918,163.918c-36.901,0-70.954-12.193-98.35-32.772",
                    "duration": 500
            }
        ],
            "dimensions": {
                "width": 270,
                "height": 336
            }
        }
    };
    var donutC = {
        "donut_c": {
            "strokepath": [
            {
                "path": "M2,2c37,0,67,30,67,67  c0,7.1-1.1,14-3.2,20.5",
                "duration": 600
            }
        ],
        "dimensions": {
            "width": 71,
            "height": 92
        }
        }
    };
    var donutD = {
        "donut_d": {
            "strokepath": [
                {
                    "path": "M2.16,2.143c90.53,0,163.918,73.389,163.918,163.918c0,67.633-40.959,125.698-99.425,150.744",
                    "duration": 500
            }
        ],
            "dimensions": {
                "width": 168,
                "height": 318
            }
        }
    };
    var donutE = {
        "donut_e": {
            "strokepath": [
            {
                "path": "M2,2c37,0,67,30,67,67  c0,23.1-11.6,43.4-29.4,55.5",
                "duration": 600
            }
        ],
        "dimensions": {
            "width": 71,
            "height": 127
        }
        }
    };
    var donutF = {
        "donut_f": {
            "strokepath": [
                {
                    "path": "M21.277,2.404c90.529,0,163.919,73.388,163.919,163.917s-73.39,163.917-163.919,163.917c-6.359,0-12.636-0.361-18.808-1.066",
                    "duration": 500
            }
        ],
            "dimensions": {
                "width": 187,
                "height": 332
            }
        }
    };

    var checkA = {
        "check_a": {
            "strokepath": [
            {
                "path": "M   4.5,21.5 L  4.5,21.5 13,30 38.5,4.5 ",
                "duration": 600
            }
        ],
        "dimensions": {
            "width": 43,
            "height": 35
        }
        }
    };

    var checkB = {
        "check_b": {
            "strokepath": [
            {
                "path": "M   4.5,21.5 L  4.5,21.5 13,30 38.5,4.5 ",
                "duration": 600
            }
        ],
        "dimensions": {
            "width": 43,
            "height": 35
        }
        }
    };

    var checkC = {
        "check_c": {
            "strokepath": [
            {
                "path": "M   4.5,21.5 L  4.5,21.5 13,30 38.5,4.5 ",
                "duration": 600
            }
        ],
        "dimensions": {
            "width": 43,
            "height": 35
        }
        }
    };


    _donutA = $('#donut_a').lazylinepainter({
        "svgData": donutA,
        "strokeWidth": 2,
        "strokeColor": "#E53281",
        'strokeCap': 'round',
        'onComplete': function () {
            _donutA.find(".percent").addClass("active");
        }
    });

    _donutB = $('#donut_b').lazylinepainter({
        "svgData": donutB,
        "strokeWidth": 4,
        "strokeColor": "#922682",
        'strokeCap': 'round',
        'responsive': true,
        'delay': 500,
        'onComplete': function () {
            _donutB.find(".percent").addClass("active");
        }
    });

    _donutC = $('#donut_c').lazylinepainter({
        "svgData": donutC,
        "strokeWidth": 2,
        "strokeColor": "#E53281",
        'strokeCap': 'round',
        'onComplete': function () {
            _donutC.find(".percent").addClass("active");
        }
    });

    _donutD = $('#donut_d').lazylinepainter({
        "svgData": donutD,
        "strokeWidth": 4,
        "strokeColor": "#922682",
        'strokeCap': 'round',
        'responsive': true,
        'delay': 500,
        'onComplete': function () {
            _donutD.find(".percent").addClass("active");
        }
    });

    _donutE = $('#donut_e').lazylinepainter({
        "svgData": donutE,
        "strokeWidth": 2,
        "strokeColor": "#E53281",
        'strokeCap': 'round',
        'onComplete': function () {
            _donutE.find(".percent").addClass("active");
        }
    });

    _donutF = $('#donut_f').lazylinepainter({
        "svgData": donutF,
        "strokeWidth": 4,
        "strokeColor": "#922682",
        'strokeCap': 'round',
        'responsive': true,
        'delay': 500,
        'onComplete': function () {
            _donutF.find(".percent").addClass("active");
        }
    });

    _checkA = $('#check_a').lazylinepainter({
        "svgData": checkA,
        "strokeWidth": 8,
        "strokeColor": "#382461",
        'strokeCap': 'round',
        'onComplete': function () {}
    });

    _checkB = $('#check_b').lazylinepainter({
        "svgData": checkB,
        "strokeWidth": 8,
        "strokeColor": "#382461",
        'strokeCap': 'round',
        'onComplete': function () {}
    });

    _checkC = $('#check_c').lazylinepainter({
        "svgData": checkC,
        "strokeWidth": 8,
        "strokeColor": "#382461",
        'strokeCap': 'round',
        'onComplete': function () {}
    });
});