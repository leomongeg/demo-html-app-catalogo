//******DEFINICION DE VARIABLES GLOBALES******//
var _bridge = false;

document.addEventListener('WebViewJavascriptBridgeReady', function (event) {
    _bridge = event.bridge;
    _bridge.playAlphaVideo = function ($element, params) {

        var position = $element.position();
        if(typeof params === 'undefined') params = {};
            
        if ((typeof params.secuence === 'undefined') && (typeof $element.data('secuence') === 'undefined')) {
            throw 'El nombre de la secuencia es requerido';
        }

        _bridge.callHandler('showVideo', {
            'x': position.left,
            'y': position.top,
            'w': $element.width(),
            'h': $element.height(),
            'resource': (typeof params.secuence === 'undefined') ? $element.data('secuence') : params.secuence
        }, function (response) {
            console.log('JS got response', response)
            if(typeof params.callback !== 'undefined') params.callback();
        });

    };

    _bridge.playFullScreenVideo = function (resource) {

        _bridge.callHandler('playFullScreenVideo', {
            'resource': resource
        }, function (response) {
            console.log('JS got response', response)
        });

    };

    _bridge.init(function (data, responseCallback) {
        //alert("Got data " + JSON.stringify(data))
        if (responseCallback) {
            //responseCallback("Todo Listo!");
        }
    });
}, false);