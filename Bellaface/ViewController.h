//
//  ViewController.h
//  Bellaface
//
//  Created by Jorge Leonardo Monge García on 1/14/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewJavascriptBridge.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController<UIWebViewDelegate, AVAudioPlayerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

